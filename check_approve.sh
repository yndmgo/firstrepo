#!/bin/bash
WHO_APPROVED_ID=$(curl -X GET https://circleci.com/api/v2/workflow/${CIRCLE_WORKFLOW_ID}/job -H "Circle-Token: ${MyToken}" | jq -r '.items[]|select(.type == "approval")|.approved_by')
WHO_APPROVED_FULL_NAME=$(curl -X GET https://circleci.com/api/v2/user/${WHO_APPROVED_ID} -H "Circle-Token: ${MyToken}" | jq -r .name)
ACCESS_GRANTED=0
GRP_MBRS=$(curl -u ${BB_admin_username}:${BB_admin_App_Password} -G "https://api.bitbucket.org/1.0/groups/yndmgo/${Approvers_Group}/members"|jq -r '.[].display_name')
IFS=$'\n'
for OUTPUT in $GRP_MBRS; do
    if [[ $OUTPUT == "${WHO_APPROVED_FULL_NAME}" ]]; then
    ACCESS_GRANTED=1
    fi
done
if [[ $ACCESS_GRANTED -eq 0 ]]; then
  echo "You are not authorized to approve this job. Cancelling workflow..."
  curl -X POST https://circleci.com/api/v2/workflow/${CIRCLE_WORKFLOW_ID}/cancel -H "Circle-Token: ${MyToken}"
  else
    echo "You are authorized to approve this job. Continuing workflow..."
fi

# Giving some time complete the cancelling before moving on to the actual job steps (if applicable)
sleep 5